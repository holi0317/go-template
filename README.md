# go-template

Template for a new go project.

## Features

We are using following tools in the project

- Editorconfig included
- Gitignore file included
- goreleaser for release
- Gitlab CI for build and release pipeline
- Makefile for some commands
- golangci-lint for additional linting

## Usage

1. Clone this repository
2. Add a new project on gitlab
3. Set origin url for the cloned repository to the new project
4. Do a global search on `go-template` and replace them with new project name
5. Rewrite this README
6. Open this project Gitlab UI. Go to Settings -> CI/CD -> Variables, add `GITLAB_TOKEN`
   as protected and masked. Get a new token from [access token] personal settings page.
7. In Gitlab settings, Repository -> Protected Tags, add `v*` as a wildcard and protect them.
   When releasing, use tags like `v1.0.0`.

[access token]: https://gitlab.com/-/profile/personal_access_tokens
