.PHONY: build
build:
	goreleaser build --snapshot --rm-dist --single-target

.PHONY: test
test:
	go test ./...

.PHONY: lint
lint:
	golangci-lint run -v

.PHONY: clean
clean:
	rm -rf target
